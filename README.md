# seed-crush

A javascript game inspired by Candy Crush, based originally on https://github.com/kubowania/candy-crush

<blockquote>

In this walkthrough, I show you how to build Candy Crush in pure JavaScript, HTML and CSS. Watch the full video walkthrough [here](https://youtu.be/XD5sZWxwJUk):

</blockquote>

Code substantially tweaked though to fit my purposes, I was challenged by my son to make a game called "seed crush". I started with [this code](https://github.com/kubowania/candy-crush) and am then fixing and replacing as I go, including:

+ linting
+ minifying
+ fixing (check the last square, match line of five)
+ refactor (merge the check for line of 3, check for line of 4 etc)
+ adding new features (only allow legal moves, hints, end of game)
+ mobile / touchscreen friendliness
+ adding static site builder for my own arcane reasons

Still working on:

+ scoring storing
+ fixing the mysterious blanks at the top

To build:

+ `npm install`
+ `npm run build`
+ `open dist/index.html`

[Play it here](https://paulypopex.gitlab.io/seed-crush/).

### MIT Licence

Copyright (c) 2020 Ania Kubow

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*Translation: Ofcourse you can use this for you project! Just make sure to say where you got this from :)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
