/* global alert, interact */
((window, document) => {
  const grid = document.querySelector('.grid')
  const scoreDisplay = document.getElementById('score')
  const width = 8
  const helpTimeout = 30 * 1000 // show a hint after 30 secs
  /* if (width !== 8) {
    grid.style.height = grid.style.width = grid.style.minWidth = (width * 70) + 'px'
  } */
  const squares = []

  const seedTypes = [
    'url(img/brown.png)',
    'url(img/brown2.png)',
    'url(img/grey.png)',
    'url(img/sunflower.png)',
    'url(img/melon.png)',
    'url(img/pumpkin.png)'
  ]

  // calculate all the squares that cannot start a row of a certain length
  const getDisallowed = length => Array.from({ length: length - 1 }, (_, i) => width - i - 1)
  const disallowed = {
    3: getDisallowed(3),
    4: getDisallowed(4),
    5: getDisallowed(5)
  }

  // cache all the valid moves
  function getValidMoves (from) {
    const column = from % width
    const validMoves = []
    if (from > width) validMoves.push(from - width)
    if (from < width * (width - 1)) validMoves.push(from + width)
    if (column > 0) validMoves.push(from - 1)
    if (column < width - 1) validMoves.push(from + 1)
    return validMoves
  }
  const validMoves = Array.from({ length: width * width }, (_, i) => getValidMoves(i))

  // makes for better minifying
  const transform = (elem, style) => {
    elem.style.webkitTransform = elem.style.transform = style
  }

  const setAttribute = (elem, key, value) => elem.setAttribute(key, value)

  const getAttributeAsNumber = (elem, key) => Number(elem.getAttribute(key))

  const getBackgroundImage = elem => elem && elem.style.backgroundImage

  const setBackgroundImage = (elem, image) => {
    elem.style.backgroundImage = image
  }

  const incrementScore = val => {
    scoreDisplay.innerHTML = Number(scoreDisplay.innerHTML) + val
  }

  const clear = index => {
    setBackgroundImage(squares[index], '')
    // be nice to add animations but then we have to maintain the values instead of inspecting the elements
    /* setBackgroundImage(squares[index], 'url(img/boom.gif)')
    setTimeout(() => {
      setBackgroundImage(squares[index], '')
    }, 200) */
  }

  const random = () => seedTypes[Math.floor(Math.random() * seedTypes.length)]

  // create your board
  for (let i = 0; i < width * width; i++) {
    const square = document.createElement('div')
    setAttribute(square, 'id', i)
    square.style.touchAction = 'none'
    setBackgroundImage(square, random())
    grid.appendChild(square)
    squares.push(square)
  }

  // enable draggables to be dropped into this
  interact('.grid div').dropzone({
    // only accept elements matching this CSS selector
    accept: '.grid div',
    overlap: 0.55,

    ondrop: function (event) {
      const draggableElement = event.relatedTarget
      const dropzoneElement = event.target
      const squareIdBeingDragged = getAttributeAsNumber(draggableElement, 'id')
      const squareIdBeingReplaced = getAttributeAsNumber(dropzoneElement, 'id')
      const colorBeingDragged = getBackgroundImage(draggableElement)
      const colorBeingReplaced = getBackgroundImage(dropzoneElement)
      // console.log('drop', squareIdBeingDragged, 'onto', squareIdBeingReplaced)
      // What is a valid move?
      const validMove = validMoves[squareIdBeingDragged].includes(squareIdBeingReplaced)
      const would = wouldMakeALine(squareIdBeingReplaced, colorBeingDragged, draggableElement)
      // console.log('validMove?', { squareIdBeingDragged, squareIdBeingReplaced, moves: String(validMoves[squareIdBeingDragged]), validMove, would })

      if (validMove && would) {
        setBackgroundImage(dropzoneElement, colorBeingDragged)
        setBackgroundImage(draggableElement, colorBeingReplaced)
      }
      // put the dragged square back
      transform(draggableElement, '')
      setAttribute(draggableElement, 'data-x', 0)
      setAttribute(draggableElement, 'data-y', 0)
    }
  })

  interact('.grid div').draggable({
    // enable inertial throwing
    // inertia: true,
    // keep the element within the area of it's parent
    modifiers: [
      interact.modifiers.restrictRect({
        restriction: 'parent',
        endOnly: true
      })
    ],

    listeners: {
      move
    },

    // enable autoScroll
    autoScroll: true
  })

  function move (event) {
    const target = event.target
    // keep the dragged position in the data-x/data-y attributes
    const x = (getAttributeAsNumber(target, 'data-x') || 0) + event.dx
    const y = (getAttributeAsNumber(target, 'data-y') || 0) + event.dy

    // translate the element
    transform(target, 'translate(' + x + 'px, ' + y + 'px)')

    // update the position attributes
    setAttribute(target, 'data-x', x)
    setAttribute(target, 'data-y', y)
  }

  function wouldJoinAColumn (id, color) {
    return squareMatchesColor(color)(id - width) && squareMatchesColor(color)(id + width)
  }

  function wouldStartAColumn (id, color) {
    return squareMatchesColor(color)(id + width) && squareMatchesColor(color)(id + width * 2)
  }

  function wouldEndAColumn (id, color) {
    return squareMatchesColor(color)(id - width) && squareMatchesColor(color)(id - width * 2)
  }

  function wouldJoinARow (id, color) {
    const column = id % width
    return (column > 0) && (column < width - 1) && squareMatchesColor(color)(id - 1) && column < width && squareMatchesColor(color)(id + 1)
  }

  function wouldStartARow (id, color) {
    const column = id % width
    return (column < width - 2) && squareMatchesColor(color)(id + 1) && squareMatchesColor(color)(id + 2)
  }

  function wouldEndARow (id, color) {
    const column = id % width
    return (column > 1) && squareMatchesColor(color)(id - 1) && squareMatchesColor(color)(id - 2)
  }

  // @todo check if the piece swapped with the dragged piece would make a line?
  function wouldMakeALine (squareIdBeingReplaced, colorBeingDragged) {
    // console.log('wouldMakeALine if', squares[squareIdBeingReplaced], 'was a', colorBeingDragged, debug, '?')
    if (wouldJoinAColumn(squareIdBeingReplaced, colorBeingDragged)) {
      console.log('✅ the middle of a column')
      return true
    }
    if (wouldStartAColumn(squareIdBeingReplaced, colorBeingDragged)) {
      console.log('✅ the top of a column')
      return true
    }
    if (wouldEndAColumn(squareIdBeingReplaced, colorBeingDragged)) {
      console.log('✅ the bottom of a column')
      return true
    }
    if (wouldJoinARow(squareIdBeingReplaced, colorBeingDragged)) {
      console.log('✅ the middle of a row')
      return true
    }
    if (wouldStartARow(squareIdBeingReplaced, colorBeingDragged)) {
      console.log('✅ the left of a row')
      return true
    }
    if (wouldEndARow(squareIdBeingReplaced, colorBeingDragged)) {
      console.log('✅ the right of a row')
      return true
    }
    return false
  }

  // drop candies once some have been cleared
  function moveIntoSquareBelow () {
    for (let i = 0; i < width * (width - 1); i++) {
      if (getBackgroundImage(squares[i + width]) === '') {
        setBackgroundImage(squares[i + width], getBackgroundImage(squares[i]))
        setBackgroundImage(squares[i], '')
      }
      const isFirstRow = i < width
      if (isFirstRow && (getBackgroundImage(squares[i]) === '')) {
        setBackgroundImage(squares[i], random())
      }
    }
  }

  const squareMatchesColor = color => index => getBackgroundImage(squares[index]) === color

  const checkForLine = (length, i, directionFunction) => {
    const color = getBackgroundImage(squares[i])
    if (color !== '') {
      const squaresToCheck = Array.from({ length }, directionFunction)
      if (squaresToCheck.every(squareMatchesColor(color))) {
        incrementScore(length)
        squaresToCheck.forEach(clear)
      }
    }
  }

  const checkForColumn = (length, i) => checkForLine(length, i, (_, index) => i + width * index)

  const checkForColumnsOf = length => {
    for (let i = 0; i <= width * (width - length + 1); i++) {
      checkForColumn(length, i)
    }
  }

  const checkForRow = (length, i) => checkForLine(length, i, (_, index) => i + index)

  const checkForRowsOf = length => {
    for (let i = 0; i <= width * width - length; i++) {
      if (disallowed[length].includes(i % width)) {
        // console.log('do not need to check', { i, length, width })
        continue
      }
      checkForRow(length, i)
    }
  }

  // Checks carried out indefintely - Add button to clear interval for best practise
  setInterval(function () {
    [5, 4, 3].forEach(v => {
      checkForRowsOf(v)
      checkForColumnsOf(v)
    })
    moveIntoSquareBelow()
  }, 100)

  // only used in debugging...
  function direction (from, to) {
    if (to < from - 1) return 'up'
    if (to > from + 1) return 'down'
    if (to < from) return 'left'
    if (to > from) return 'right'
    return '???'
  }

  // can we help?
  const helpInterval = setInterval(function () {
    for (let i = squares.length - 1; i >= 0; i--) {
      for (let j = validMoves[i].length - 1; j >= 0; j--) {
        const neighbourIndex = validMoves[i][j]
        const neighbour = squares[neighbourIndex]
        const color = getBackgroundImage(neighbour)
        let ok = false
        if ([i - 1, i + 1].includes(neighbourIndex) && (wouldJoinAColumn(i, color) || wouldStartAColumn(i, color) || wouldEndAColumn(i, color))) {
          console.log('✅ move', neighbour, 'horizontally and form a column')
          ok = true
        }
        if ([i - width, i + width].includes(neighbourIndex) && (wouldJoinARow(i, color) || wouldStartARow(i, color) || wouldEndARow(i, color))) {
          console.log('✅ move', neighbour, 'vertically and form a row')
          ok = true
        }
        if ([i + 1].includes(neighbourIndex) && wouldEndARow(i, color)) {
          console.log('✅ move', neighbour, 'left and end a row')
          ok = true
        }
        if ([i - 1].includes(neighbourIndex) && wouldStartARow(i, color)) {
          console.log('✅ move', neighbour, 'right and start a row')
          ok = true
        }
        if ([i + width].includes(neighbourIndex) && wouldEndAColumn(i, color)) {
          console.log('✅ move', neighbour, 'up and end a column')
          ok = true
        }
        if ([i - width].includes(neighbourIndex) && wouldStartAColumn(i, color)) {
          console.log('✅ move', neighbour, 'down and start a column')
          ok = true
        }
        // can't move vertically and become the middle of a column,
        // or horizontally and become the middle of a row...
        if (ok) {
          neighbour.style.animation = 'rotation 1s infinite linear'
          setTimeout(() => {
            neighbour.style.animation = ''
          }, 1000)
          console.log('✅', neighbour, direction(neighbourIndex, i), 'makes a line')
          return 0
        }
      }
    }
    alert('looks like game over! refresh to start again')
    clearInterval(helpInterval)
  }, helpTimeout)
})(window, document)
