const babel = require('@babel/core')

module.exports = options => {
  return (files, metalsmith, done) => {
    setImmediate(done)
    Object.keys(files).forEach(file => {
      if (!/\.js$/.test(file)) return
      const result = babel.transformSync('' + files[file].contents)
      if (!result) return
      files[file].contents = result.code
    })
  }
}
